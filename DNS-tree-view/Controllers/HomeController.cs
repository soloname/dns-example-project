﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DNS_tree_view.Models;
using DNS_tree_view.Models.ViewModal;
using DNS_tree_view.Models.ApiExtensions;
using System.Web.Script.Serialization;
using System.Data;

namespace DNS_tree_view.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {           
            return View();
        }
              
       
      
        public ActionResult GetListForTree()
        {
            try
            {
                DNS_DB_exampleEntities db = new DNS_DB_exampleEntities();
                var listElementTree = db.ElementTree.ToList();
                List<ElementTreeViewModal> ListViewModal = new List<ElementTreeViewModal>();

                var q = listElementTree.GroupBy(p => p.ElementTree___id);
                var roots = q.Where(p => p.Key == null).First();

                foreach (var root in roots)
                {
                    ListViewModal.Add(root.ToViewModel(db));
                }
                return Json(new { success = true, json = ListViewModal }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false });
            }
        }
      
        [HttpPost]
        public ActionResult PostTransferElementTree(int parent_id, int children_id)
        {
            try
            {
                DNS_DB_exampleEntities db = new DNS_DB_exampleEntities();
                var children = db.ElementTree.Find(children_id);
                children.ElementTree___id = parent_id;
                db.Entry(children).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false });
            }  
        }
        
    }
}