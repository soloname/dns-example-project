﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DNS_tree_view.Models;
namespace DNS_tree_view.Models.ViewModal
{
    public class ElementTreeViewModal
    {
        public decimal id { get; set; }
        public string Name { get; set; }
        public TypeElementTree TypeEnum { get; set; }
        //список дочерних элементов 
        public List<ElementTreeViewModal> ElementsTree { get; set; }
    }
}