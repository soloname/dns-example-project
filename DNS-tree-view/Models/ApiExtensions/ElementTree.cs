﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using DNS_tree_view.Models;
using DNS_tree_view.Models.ViewModal;

namespace DNS_tree_view.Models.ApiExtensions
{
    public static class ElementTreeExtensions
    {
     

        public static ElementTreeViewModal ToViewModel(this ElementTree e, DNS_DB_exampleEntities db)
            {

            List<ElementTreeViewModal> ChildrenList = new List<ElementTreeViewModal>();
            foreach(var el in e.ElementTree1.Select(s=>s.ToViewModel(db)))
            {
                ChildrenList.Add(el);
            }
            return new ElementTreeViewModal
            {
                    id = e.Id,
                    Name = e.Name,
                    TypeEnum = e.Type___id == 1 ? TypeElementTree.Folder : TypeElementTree.File,   
                    ElementsTree = ChildrenList

            };
            }
        }
}